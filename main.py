def func_n(N, k=0):
    if N == 1:
        return k
    elif N >1:
        N = N / 2
        k += 1
        return func_n(N,k)
    else:
        print('Это не степень двойки')

if __name__ == '__main__':
    N = int(input("Введите натуральное число:"))
    print(func_n(N))