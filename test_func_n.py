from main import func_n
import pytest

@pytest.mark.parametrize("N,result", [(2, 1), (16,4)])
def test_good(N,result):
    assert func_n(N) == result


def test_negativ():
    func_n(34)